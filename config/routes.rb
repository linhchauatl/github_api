Rails.application.routes.draw do
  resources :git_users, only: [:index, :update]
  resources :react_users
end
