class CreateGitUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :git_users do |t|
      t.string :login
      t.boolean :favorite

      t.timestamps
    end
  end
end
