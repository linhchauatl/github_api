require 'rails_helper'

describe GitUserService do
  context 'list_git_users' do
    it 'returns a list of git users' do
      users = GitUserService.list_git_users
      expect(users.is_a? Array).to be true
      users.each do |user|
        expect(user['login']).not_to be nil
        expect(user['avatar_url']).not_to be nil
        expect(user['favorite']).not_to be nil
      end
    end
  end
end
