require 'rails_helper'

RSpec.describe GitUsersController, type: :controller do
  context 'index' do
    it 'returns a list of github users' do
      result = get :index
      users = controller.instance_variable_get(:@users)
      expect(result).not_to be nil
      expect(users).not_to be nil
      expect(users.any?).to be true
    end
  end

  context 'update' do
    let(:jackie_chan) { FactoryGirl.create(:user, login: 'jackie_chan', favorite: false) }

    it 'creates a new user with correct favorite if user does not exist' do
      response = put :update, params: { id: 'jet_li', login: 'jet_li', checked: true }
      expect(response.status).to eql(200)

      git_user = GitUser.find_by_login('jet_li')
      expect(git_user).not_to be nil
      expect(git_user.favorite).to be true
    end

    it 'updates favorite of existing user correctly' do
      response = put :update, params: { id: 'jackie_chan', login: 'jackie_chan', checked: true }
      expect(response.status).to eql(200)

      git_user = GitUser.find_by_login('jackie_chan')
      expect(git_user).not_to be nil
      expect(git_user.favorite).to be true
    end
  end
end
