FactoryGirl.define do
  factory :git_user do
    login "login_#{Time.now.to_f}"
    favorite false
  end
end
