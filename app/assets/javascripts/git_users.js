function saveFavorite(login) {
  console.log($('#' + login).is(':checked'))
  var request = $.ajax(
    {
      url: "/git_users/" + login,
      beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name=csrf-token]').attr('content') ) },
      method: "PUT",
      data: { "login": login, "checked": $('#' + login).is(':checked') },
      dataType: "json"
    }
  )

  request.done(function( msg ) {
    alert(msg['message'])
  })

  request.fail(function( jqXHR, textStatus ) {
    alert(jqXHR.responseJSON['message'])
  })
}
