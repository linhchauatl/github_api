class GitUsersController < ApplicationController
  def index
    @users = GitUserService.list_git_users
  end

  def update
    git_user = GitUser.where(['login = ?', params[:login]]).first
    if git_user
      git_user.update(favorite: params[:checked])
    else
      GitUser.create(login: params[:login], favorite: params[:checked])
    end

    render json: { message: :success}
  end
end
