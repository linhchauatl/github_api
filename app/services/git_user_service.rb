class GitUserService
  class << self

    def list_git_users
      result = RestClient.get('https://api.github.com/users')
      users = JSON.parse(result.body)
      git_users = GitUser.all.group_by(&:login)
      users.each do |user|
        git_user = git_users[ user['login'] ].try(:[], 0)
        user['favorite'] = git_user.try(:favorite) || false
      end
      users
    end
    
  end
end
